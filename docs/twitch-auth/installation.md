---
sidebarDepth: 2
---

# Installation

## Unreal Engine Marketplace

The easiest way to install the plugin is over the [Unreal Engine 4 Marketplace](https://www.unrealengine.com/marketplace).

::: warning
When you decide to install it over the Unreal Engine 4 Marketplace the plugin will be installed as a **Engine Plugin**.
:::

## Install as Engine Plugin

### Engine Plugin Folder

:::warning
Please close any running instances of Unreal Engine 4 Editors when install the plugin.
:::

The unzipped content of the previously downloaded release have to be put into the plugin folder of your Unreal Engine 4 installation.

If you didn't change the default settings while installing Unreal Engine 4 you can find the folder here: `C:\Program Files\Epic Games\UE_4.19\Engine\Plugins`.

Simply move the extracted files from the downloaded release into the above mentions plugins folder.

### Enabling The Plugin

Once the plugin is in the correct plugin folder the engine will automatically recognize the new plugin on startup.

![NewPlugin](/images/twitch-auth/NewPlugin.png)

Click on "**Manage Plugins...**" and enable it.

![EnablePlugin](/images/twitch-auth/EnablePlugin.png)

That's it now you're all set and can start using it.

## Install as Project Plugin

:::warning
Please close any running instances of Unreal Engine 4 Editors when install the plugin.
:::

### Project Plugin Folder

The unzipped content of the previously downloaded release have to be put into the plugin folder of your project.

::: tip
If your project does not have a `Plugin` folder you can simply create it.
:::

Simply move the extracted files from the downloaded release into the above mentions plugins folder.

### Enabling the Plugin

Once the plugin is in the correct plugin folder the engine will automatically recognize the new plugin on startup.

![NewPlugin](/images/twitch-auth/NewPlugin.png)

Click on "_Manage Plugins..._" and enable it.

![EnablePlugin](/images/twitch-auth/EnablePlugin.png)

That's it now you're all set and can start using it.
