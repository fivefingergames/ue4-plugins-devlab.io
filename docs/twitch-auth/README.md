---
sidebarDepth: 2
---

# Introduction

## What Is It?

**Twitch Auth** is a Unreal Engine 4 plugin that enable game developers, especially Twitch game developers, to get specifiy in-game content to their subscribers.

## Idea & Use Case

The idea for this plugin came while watching the development stream from a fellow Twitch streamer called [Wheeze202](https://www.twitch.tv/wheeze202). He provides his Twitch subscribers early development builds of his current project [Galdur](https://store.steampowered.com/app/882660/Galdur/) to playtest and give him feedback on. His infrastructure was simple, he simply whispered the link to the build when a subscriber entered a command in the Twitch chat.

I saw some flaws in this approach, because now you had to rely on the subscriber to keep the link a secret. I though there must be a better way. So, I set out to build this plugin to let any Twitch game developer have the ability to let potential subscriber play testers authenticate in-game. Now only subscribers to your channel can play the game.

Not only can you use it as a custom Twitch DRM, but you can also distribute in-game content to your most loyal viewers.
