---
sidebarDepth: 2
---

# Creating Managed C++ Component

## Inheriting From `ManagedActorComponent`

## Creating `ManagedActorComponent` From Scratch

### Implementing The Component Lifetime Hooks

#### BeginPlay

#### OnComponentCreated

#### OnComponentDestroyed

#### OnRegister
