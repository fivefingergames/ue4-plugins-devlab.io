---
sidebarDepth: 3
---

# Component Dependencies

Dependencies are the one aspect that you only need to worry about once when you decide what you need, everything else is handled by **Coma**.

## What Are Component Dependencies?

Component dependencies are the components your **Managed Component** needs to fullfill its responsibilities.

Let's take a look on how to define them inside your [previously](/docs/coma/blueprints/managed-components) created **Managed Component**.

## How To Add Component Dependencies?

### Our Test Component Dependencies

To add dependencies we of course need components that will serve as our dependencies.

To keep it simple just create two components:

-   `BP_FooComponent`
-   `BP_BarComponent`

You might have guessed what they will do, for now they will both have a public function that will print to the screen.

> You can probably guess what the `BP_BarComponent` looks like.

![FooComponent](/images/coma/FooComponent.png)

### Adding Test Component Dependencies

Now that we got our test dependencies we can add them to the **Managed Component**.

Let's open up the [previously](/coma/blueprints/managed-components) created **Managed Component** and take a look at the **Details Panel**.

![ManagedComponentDetailsPanel](/images/coma/ManagedComponentDetailsPanel.png)

Under the **Coma** section you can see two properties that the **Managed Component** comes with.

![ManagedComponentsProperties](/images/coma/ManagedComponentsProperties.png)

**Let's take a look at them in more detail:**

#### Destroy Dependencies on Own Destruction

This is a simple boolean flag which indicates whether the **Managed Component** will destroy its dependencies when itself is removed/destroyed.

#### Dependencies

This property is the heart of the **Managed Component** here you can add and name all your components the managed component depends on.

Let's add our test dependencies.

First we create a new entry on the **Dependencies** map property.~

![AddDependency](/images/coma/AddDependency.png)

We name the dependency `Foo` and select the `BP_FooComponent`.

![FooComponentDependency](/images/coma/FooComponentDependency.png)

And do the same for the `BP_BarComponent`.

![FooBarComponentDependencies](/images/coma/FooBarComponentDependencies.png)

### Dependency Variables

Now, here is where the magic starts.

When you now compile the Blueprint **Coma** will automatically create and inject variables of the correct type.

![DependencyVariables](/images/coma/DependencyVariables.png)

### Working With Dependencies

Now you can safely work with the created dependency variables and use them in your **Managed Component**.

![UsingDependencies](/images/coma/UsingDependencies.png)

You might be wondering from where these variables will receive their value.

We will look at that in the next section, for now you can use the variables as valuesless placeholders for a future dependency component reference.
