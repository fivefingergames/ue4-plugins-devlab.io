---
sidebarDepth: 3
---

# Managed Components

To jump into **Coma** we need to get to know the main component (pun intended) of the plugin which is the **Managed Component**.

## What Is a Manged Component?

First of we need to clearify what a **Managed Component** is.
A **Managed Component** is the component where you, as the developer, create the component dependency graph and use you required component dependencies.

::: warning
As of right now **Coma** only supports **Actor Components** as **Managed Components**.
:::

This is a very abstract definition, but you will see what exactly that means in a just a minute.

## How To Create a Managed Component?

Let's get started creating a **Managed Component**.

To create a Blueprint **Managed Component** we first start as we would with any other Blueprint we want to create.

> Right Click -> Create Basic Asset/Blueprint Class

![CreateBlueprint](/images/coma/CreateBlueprint.png)

Once the **Pick Parent Class** dialog is open you can discard the **Common Classes** section and go straight into the **All Classes** section.

![PickParentClassDialog](/images/coma/PickParentClassDialog.png)

Here in the **All Classes** section you can begin to search for **_managed_**...

If only **Coma** is installed it will yield one result right under the the **Actor Component** entry.

![PickManagedComponent](/images/coma/PickManagedComponent.png)

Choose that entry (`ManagedActorComponent`) for your new Blueprint.

![SelectParentClass](/images/coma/SelectParentClass.png)

Once selected you can go ahead and name your new Blueprint **Managed Component**, you know the drill.
