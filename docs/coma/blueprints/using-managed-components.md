---
sidebarDepth: 2
---

# Using Managed Components

Now that we [created and populated a **Manged Component**](/docs/coma/blueprints/defining-dependencies.md) we can get down to business and use it.

## Creating a Host Actor

But first we need to create a **Actor** to add our [previously created](/docs/coma/blueprints/managed-components.md) **Managed Component** to.

Assuming you already know how to create a **Actor** inside the Unreal Engine 4 editor we will skip this step. If not refer to [this documentation](https://docs.unrealengine.com/en-US/Engine/Blueprints/UserGuide/Types/ClassBlueprint/Creation/index.html).

A Blueprint **Actor** called **BP_Actor** has been created for this guide in will be used from here on out.

![HostActor](/images/coma/HostActor.png)

## How To Use a Managed Component?

So, now we come to the best part of this guide, actually using our [previously created](/docs/coma/blueprints/managed-components.md) and [defined dependencies](/docs/coma/blueprints/defining-dependencies.md).

### Adding a Managed Component To The Host Actor

To add and use our **Managed Component** we first need to open the host **Actor** and add it to the component list of it.

To do this open the **Actor** in the full Blueprint editor.

![FullBlueprintEditor](/images/coma/FullBlueprintEditor.png)

Once the full blown Blueprint editor is open navigate to the upper left corner of the editor to the **+ Add Component** button.

![AddComponentButton](/images/coma/AddComponentButton.png)

When clicked it will reveal a dropdown with all available components, so we search for our previously created component.

![SelectComponent](/images/coma/SelectComponent.png)

Add your **Managed Component** and name it what ever you want.

![AddComponent](/images/coma/AddComponent.png)

Now we enter another magic moment in the world of **Coma** once you submit your component name or compile the Blueprint for the first time, it's defined dependencies will appear automatically in the components list.

### Working With a Managed Component And Depedencies
