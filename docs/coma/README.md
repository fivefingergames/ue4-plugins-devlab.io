---
sidebarDepth: 2
---

# Introduction

## What Is It?

**Coma** - **Co**mponent **Ma**nager is a Unreal Engine 4 plugin which enables C++ and Blueprint developers alike to model component dependencies more easily and efficiently.

## Idea & Use Case

While we were working on our new game [**Knights of Cathena**](https://steemknights.com/) we used components extensively. We used them in the context of the SOLID principle which allows each component to only have a single responsibility and reference another component for another responsibility if a call to it is needed to.

The approach we took was very combersome, we abstracted the logic to fetch the needed component references for another responsibility within a method and printed out a error and halted the game if the needed component couldn't be found on the parent actor. You can imagine that this took a lot of boilerplate code and maintenance, because of that we lost our precious iteration time that we all need to create great gameplay features.

So, we sat out to create a better and less boilerplate solution that would not only work for our C++ gameplay code but for Blueprint as well. And **Coma** raised from the ashes!
