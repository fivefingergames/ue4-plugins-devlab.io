---
sidebarDepth: 2
---

# Introduction

## What Is It?

**UPromises** is an Unity plugin that brings JS-like Promises to Unity.

## Idea & Use Case

Since the release of Unity2017 Developers were able to use the powerful async/await feature in Unity.
This allows developers to easily combine asynchronous tasks without the need of CoRoutines to do the heavy lifting. As async/await does allow for arbitrary return values without the need of "global" variables this results in a much cleaner and easier to understand codebase.

However the Task class C# provides isn't exactly easy to use and misses a few core features one might expect if coming from JS knowing their approach with promises.

For this reason we made JS-like Promises in C#!

## Installation via Unity Assetstore

The easiest way to install the plugin is over the [Unity Assetstore](https://assetstore.unity.com/packages/slug/179091).

After the installation you are ready to start developing your games using our promises!
