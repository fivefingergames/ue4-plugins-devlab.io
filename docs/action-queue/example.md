---
sidebarDepth: 2
---

# Complete Example

Here we will explore a complete example of the system.
We will create a [`Action`](/action-queue/action) which prints some message to the screen after **n** amount of seconds.
Let's get started!

::: warning
**NOTE**

This assumes that you know how to create a [`Sub Action`](/action-queue/sub-action), [`Action`](/action-queue/action) Blueprint class.

If your don't know how to create these head over to the respected sections and read them first.
:::

## Sub Action Implementation

It's always a good idea to start with the [`Sub Action`](/action-queue/sub-action) which actually hold the behaviour to execute.

::: tip
**Our intended behaviour should print some message to the screen after _n_ amount of seconds.**
:::

Let's dissect that.

-   `Seconds` is a indication that this is a behaviour that needs to implement most of the logic in the `Tick` event
-   `Message` and `n Amount` is a indication that these values should be dynamically set before execution

### Sub Action Variables

As we dissected the specifications for our behaviour we need to create the necessary variables in the Blueprint class.

#### Timer

At first we add the `Timer` variable which is just a simple float to count up to the intended amount of seconds (`Delay`) we should wait to print the `Message`.

![ExampleTimerVariable](/images/action-queue/ExampleTimerVariable.png)

#### Message

Next, we need to store the `Message` we want to print to the screen after the set amount of seconds (`Delay`) have elapsed.

This variable needs to be **Editable** and **Exposed on spawn**.

![ExampleMessageVariable](/images/action-queue/ExampleMessageVariable.png)

#### Delay

At last, we need to store the `Delay` after which we want to print the `Message` to the screen.

This variable needs to be **Editable** and **Exposed on spawn** as well.

![ExampleDelayVariable](/images/action-queue/ExampleDelayVariable.png)

### Sub Action Events

Alright, now that we got all the variables let's get to business implementing the actual behaviour.

#### Execute Event

Here we are setting up all the necessary things for the `Tick` event.

This includes:

-   Reset of the [`Timer`](/action-queue/example.html#timer) variable
-   An changing the [`Sub Action`](/action-queue/sub-action) to `Can Tick = true`
-   Just to be friendly, we also print a little message to the screen to tell the player that we started execution

That's all for the `Execution` event. Moving right along!

![ExampleExecuteEvent](/images/action-queue/ExampleExecuteEvent.png)

#### Tick Event

So, now to the `Tick` event, this is where all the magic happens.

Here we need to keep track of the `Time` and print the given `Message` after the set amount of `Delay` has elapsed.
After we did all of that we also need to explicitly tell the [`Sub Action`](/action-queue/sub-action) that we are **done** executing!

To keep track of time we just increment the [`Timer`](/action-queue/example.html#timer) variable every tick by the `Delta Time`.

After every increment we check if we are equal or greater then the set `Delay` on the [`Sub Action`](/action-queue/sub-action).

![ExampleTickEventTime](/images/action-queue/ExampleTickEventTime.png)

Now, to the actual behaviour implementation.
We use the check after every increment (see above) to branch to the execution condition and print the [`Message`](/action-queue/example.html#message).

To be save we also include a `Do Once` node, so that we can be sure the path is only executed once. Since we are in a **ticking** execution path.

![ExampleTickEventPrint](/images/action-queue/ExampleTickEventPrint.png)

Finally we need to tell the [`Sub Action`](/action-queue/sub-action) that we are **done** executing our behaviour.
Nothing easier then that!

![ExampleTickEventDone](/images/action-queue/ExampleTickEventDone.png)

That's the [`Sub Action`](/action-queue/sub-action) completed.
As the complete implementation is broken up above, here is the complete `Tick` event implementation.

![ExampleTickEventComplete](/images/action-queue/ExampleTickEventComplete.png)

## Action Implementation

The next logical step is to implement the [`Action`](/action-queue/action) which utilizes the above created [`Sub Action`](/action-queue/sub-action).

That's actually the simplest Blueprint of all, as this only constructs the above created [`Sub Action`](/action-queue/sub-action) and adds it to itself.

It also adds the dynamic values for the [`Sub Action`](/action-queue/sub-action) on construction.

![ExampleSetupEvent](/images/action-queue/ExampleSetupEvent.png)

::: warning
**NOTE**

This example is intended to be as simple as possible!

So, you can get a lot fancier here, pass in [`Sub Action`](/action-queue/sub-action) from the outside, construct them based on variables in the [`Action`](/action-queue/action).
You get the idea.
:::

## Action Queue Component Host Implementation

::: warning
**NOTE**

You **do not** need to use the [`Action Queue Component`](/action-queue/action-queue-component) for [`Action`](/action-queue/action) storage.
It's just part of this example to show the complete API.
:::

Finally we need to setup a [`Action Queue Component`](/action-queue/action-queue-component) host.
For this we use a simple `Actor` that we can place in a level.

Let's start by adding the [`Action Queue Component`](/action-queue/action-queue-component) to the host `Actor`.

![AddActionQueueComponent](/images/action-queue/AddActionQueueComponent.png)

### Begin Play Event

When the [`Action Queue Component`](/action-queue/action-queue-component) is added we can start by populating it with the above created [`Action`](/action-queue/action) on the `Begin Play` event.

This is very easy to do, we just construct the above created [`Action`](/action-queue/action) and `Enqueue` it to the [`Action Queue Component`](/action-queue/action-queue-component).

![ExampleActionEnqueue](/images/action-queue/ExampleActionEnqueue.png)

### Execution

Finally we want to execute the above created system.
In this example we just added a `Q` event which fires when the player is pressing **Q** on the keyboard.

::: warning
**NOTE**

To be able to use the `Q` event in the `Actor` you need to setup the `Actor` to recieve input.
:::

Here we just need to `Dequeue` and `Execute` the above added [`Action`](/action-queue/action).

This can be achived in two ways:

![ExampleActionDequeueAndExecuteSep](/images/action-queue/ExampleActionDequeueAndExecuteSep.png)

**_OR_**

![ExampleActionDequeueAndExecute](/images/action-queue/ExampleActionDequeueAndExecute.png)

## Final Result

That's it, now if you press **Q** it will execute!
This is the final result.

![Example](/images/action-queue/Example.gif)
