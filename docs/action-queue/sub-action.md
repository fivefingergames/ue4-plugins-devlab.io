---
sidebarDepth: 2
---

# Sub Action

## What Is It?

A `Sub Action` is the work horse of the plugin.

It will hold all your actually executed code which does all the heavy-lifting.

## How To Use?

### Creating a Sub Action

Let's create a new `Sub Action` from scratch.

To create a new `Sub Action` just start by creating a new blueprint by right clicking into the **Content Browser** and choose **Blueprint Class**.

![AddActionContextMenu](/images/action-queue/AddActionContextMenu.png)

In the **Pick Parent Class** dialog, skip the **Common Classes** and move down to **All Classes**.
Enter **Sub Action** and choose the class inheriting directly from **Object**.

![AddSubActionParentClassDialog](/images/action-queue/AddSubActionParentClassDialog.png)

After you choose the correct parent class create the Blueprint and name it to your liking.

### Sub Action API

The `Sub Action` API is actually not that complex, as the class is only meant to aid you in implementing your own custom behaviour.
Let's get an overview.

![SubActionAPI](/images/action-queue/SubActionAPI.png)

### Execution

You can react to the execution of a `Sub Action` by implementing the `Execute` event.

If the `Sub Action` behaviour **tick-independent** you can implement your whole logic on this event. Otherwise you would use the `Execute` event to setup all the variables you need to for a tickable behaviour.

This example resets a timer variable and prints something to the screen.

![SubActionExecute](/images/action-queue/SubActionExecute.png)

### Ticking

Most of the time you want your `Sub Action` to have a tickable behaviour.
For this to achive there is a `Tick` event.

This example continues on the example above and adds to the timer variable every `Tick`.

![SubActionTick](/images/action-queue/SubActionTick.png)

To be able to tick you need to call the `Can Tick` node, this is usually done in the `Execute` event as part of the ticking setup.

This example continues on the [`Execution`](/action-queue/sub-action.html#execution) section's example.

![SubActionCanTick](/images/action-queue/SubActionCanTick.png)

### Lifetime Nodes

#### Done

To tell the parent [`Action`](/action-queue/action) that a `Sub Action` is done executing, you need to explicitly call the `Done` node.

![SubActionDone](/images/action-queue/SubActionDone.png)

#### Cancellation

A `Sub Action` can be cancelled at anytime, although this will also cancel the parent [`Action`](/action-queue/action).

![SubActionCancel](/images/action-queue/SubActionCancel.png)

::: warning
**NOTE**

When a `Sub Action` is cancelled it will automatically cancel the parent [`Action`](/action-queue/action).
:::

#### On Cancel Event

All bound custom events or event dispatchers will be called when the event `On Cancel` executes.

This is mainly used to let other gameplay object know that something went wrong. As the cancellation is propagated to the parent [`Action`](/action-queue/action) you could also use the `On Cancel` event there.

![SubActionOnCancel](/images/action-queue/SubActionOnCancel.png)

::: warning
**NOTE**

It is recommended to use the `On Cancel` API from the parent [`Action`](/action-queue/action) as the cancellation will be propagated anyways.
:::
