---
sidebarDepth: 2
---

# Introduction

## What Is It?

**Action Queue** is a Unreal Engine 4 plugin that let's you create simple action/task sequences without the complexity of a behavior tree.

## Idea & Use Case

Did you ever want to create a sequence of actions/tasks that are unrelated to Artificial Intelligence but wanted to use the familiar interfaces of executing small actions/tasks one after another to create a bigger action/tasks?

Well, welcome to **Action Queue** the Unreal Engine 4 plugin which let's you create just that.

::: warning
**TERMINOLOGY**

The documentation will refer to **actions** only from here on out. This is to keep the confusion to a minimum, the mix up with **tasks** has been used to illustrate the similarity to the **tasks-system** from behavior trees.

**DOCUMENTATION AID**

We tried to link to all contextual sections as much as possible, all API objects are always denoted with this `Syntax`, most of the time these are direct links to each of the different sections.
:::
