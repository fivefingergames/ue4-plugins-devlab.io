---
sidebarDepth: 2
---

# Action

## What Is It?

To put it simply, a `Action` is only a shell which is responsible for setting, holding and executing [`Sub Actions`](/action-queue/sub-action).

## How To Use?

### Creating a Action

Let's create a new `Action` from scratch.

To create a new `Action` just start by creating a new blueprint by right clicking into the **Content Browser** and choose **Blueprint Class**.

![AddActionContextMenu](/images/action-queue/AddActionContextMenu.png)

In the **Pick Parent Class** dialog, skip the **Common Classes** and move down to **All Classes**.
Enter **Action** and choose the class inheriting directly from **Object**.

![AddActionParentClassDialog](/images/action-queue/AddActionParentClassDialog.png)

After you choose the correct parent class create the Blueprint and name it to your liking.

### Action API

The `Action` API is quite a bit bigger and more complex then the [`Action Queue Component`](/action-queue/action-queue-component).

Let's get an overview.

![ActionAPI](/images/action-queue/ActionAPI.png)

### Setup Event & Adding Sub Actions

The `Setup` event is usually used to setup all [`Sub Actions`](/action-queue/sub-action) belonging to the `Action`. Altough, it can be used for other purposes as well.
One thing to keep in mind is that the `Setup` event is called right before the execution starts.

In the following example it is used to construct a [`Sub Action`](/action-queue/sub-action), which is the usual way to go.

![ActionSetupEventExample](/images/action-queue/ActionSetupEventExample.png)

::: warning
**NOTE**

The `Setup` event does not always have to be used like shown above.
You are able to add [`Sub Actions`](/action-queue/sub-action) at anytime before execution.

`Setup` is just the last time where you would be able to add any [`Sub Actions`](/action-queue/sub-action).
:::

### Lifetime Events & Nodes

#### Execution

What is your `Action` worth if there is no way to execute it.
Fortunately, there is a public node to start executing a `Action`.
If you start executing the [`Sub Actions`](/action-queue/sub-action) will be executed in the order you added them to the `Action`.

This example uses the [`Action Queue Component`](/action-queue/action-queue-component) to dequeue and execute a `Action`.

![ActionExecute](/images/action-queue/ActionExecute.png)

::: warning
**NOTE**

The `Setup` event is called just before execution start.
:::

#### Cancellation

What if your game takes a completely different turn and the last executed `Action` need to stop executing.
Nothing easier then that, just use the `Actions` public `Cancel` node.

![ActionCancel](/images/action-queue/ActionCancel.png)

::: warning
**NOTE**

Refer to the [Clearing Sub Actions](/action-queue/action.html#clearing-sub-actions) section for possible side effects.
:::

### Lifetime Management

#### Clearing Sub Actions

As [`Sub Actions`](/action-queue/sub-action) are only used for one execution within a `Action` they are discarded when finished executing, altough if you cancel a `Action`, un-executed [`Sub Actions`](/action-queue/sub-action) may still be lurking around when it comes to executing the `Action` again.

![ActionClearSubActions](/images/action-queue/ActionClearSubActions.png)

::: warning
**NOTE**

If you are planning to reuse a `Action` multiple times or it is subject to cancellation while executing you should use the `Clear Sub Actions` node.
:::

#### On Done Event

All bound custom events or event dispatchers will be called when the event `On Done` executes.

This is mainly used to start dequeuing the next `Action` when you use the [`Action Queue Component`](/action-queue/action-queue-component).

![ActionOnDone](/images/action-queue/ActionOnDone.png)

#### On Cancel Event

All bound custom events or event dispatchers will be called when the event `On Cancel` executes. This can be used to notify an array of other gameobjects that the executing `Action` has been cancelled.

This example, again uses the [`Action Queue Component`](/action-queue/action-queue-component), here we just simply print to the screen.

![ActionOnCancel](/images/action-queue/ActionOnCancel.png)

### Utilities

#### Get Executing Sub Action

Maybe you need to change data in an already executing [`Sub Action`](/action-queue/sub-action) this can be archived by simply retrieving the currently executing [`Sub Action`](/action-queue/sub-action).

![ActionGetExecutingSubAction](/images/action-queue/ActionGetExecutingSubAction.png)

#### Is Executing

If you passing around actions and you are not sure it the `Action` in question is currently executing, use this.

![ActionIsExecuting](/images/action-queue/ActionIsExecuting.png)
