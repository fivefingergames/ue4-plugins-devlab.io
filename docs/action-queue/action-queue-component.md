---
sidebarDepth: 2
---

# Action Queue Component

## What Is It?

To make it easier to execute [`Actions`](/action-queue/action) in a queue-ish fashion the plugin comes with a `Action Queue Component` that can be used to queue up [`Actions`](/action-queue/action) and enqueue [`Actions`](/action-queue/action) you want to execute.

::: warning
You **can** execute actions without the usage of the `Action Queue Component`. It's just a included storage possibility.  
:::

## How To Use?

### Adding The Component

To use the `Action Queue Component` just add it to a host actor, player controller, character, or where ever you want to hold and execute [`Actions`](/action-queue/action).

![AddActionQueueComponent](/images/action-queue/AddActionQueueComponent.png)

### Component API

The `Action Queue Component` API is intentionally very slim, it's meant to only hold [`Actions`](/action-queue/action) after all.

![ActionQueueComponentAPI](/images/action-queue/ActionQueueComponentAPI.png)

Let's break this down a bit.

### Enqueuing

The `Action Queue Component` let's you queue up actions.

In this example it just constructed one and enqueued it. However, [`Actions`](/action-queue/action) can be created somewhere else and be passed in.

![ActionQueueComponentEnqueue](/images/action-queue/ActionQueueComponentEnqueue.png)

### Dequeuing

We have two possible ways to dequeue [`Actions`](/action-queue/action), one where only the [`Action`](/action-queue/action) is passed back and another where the [`Action`](/action-queue/action) is executed right before it's passed back.

This gives you greater flexibility over how and when to execute. If you simply want to dequeue and execute the component does it all for you. However, if you want to add additional data to the [`Action`](/action-queue/action) being dequeued you can simply dequeue it and execute it yourself.

#### Dequeue Only

![ActionQueueComonentDequeue](/images/action-queue/ActionQueueComonentDequeue.png)

#### Dequeue & Execute

![ActionQueueComponentDequeueAndExecute](/images/action-queue/ActionQueueComponentDequeueAndExecute.png)

### Utilities

Finally the `Action Queue Component` provides some utilities so make your life easier.

#### Executing Action

If you used [Dequeue & Execute](/action-queue/action-queue-component.html#dequeue-execute) you don't have to cache the executing action yourself, the component keeps track of it.
You can simply retrieve it via the pure node `Get Executing Action`.

![ActionQueueComponentExecutingAction](/images/action-queue/ActionQueueComponentExecutingAction.png)

::: danger
**DANGER ZONE**

Be carful here, if you use a mix of `Dequeue` and `Dequeu and Execute` it is possible that the returned [`Action`](/action-queue/action) is a relict from a older `Dequeue and Execute` or is invalid in the first place.
:::

#### Is Empty

To be save to access a dequeued action you can either check if it's valid or use the provided utility function `Is Empty`.

![ActionQueueComponentIsEmpty](/images/action-queue/ActionQueueComponentIsEmpty.png)
