module.exports = {
    title: "fivefingergames Plugins",
    description: "Complete documentation for all Unreal Engine 4 Plugins",
    head: [["link", { rel: "icon", href: "/images/Hand.svg" }]],
    dest: "public",
    ga: "UA-144436890-1",
    serviceWorker: true,
    themeConfig: {
        logo: "/images/Hand.svg",
        nav: [
            {
                text: "Docs",
                ariaLabel: "Docs Menu",
                items: [
                    { text: "Overview", link: "/docs/" },
                    { text: "Twitch Auth", link: "/docs/twitch-auth/" },
                    { text: "Coma - Component Manager", link: "/docs/coma/" },
                    { text: "UPromises - Ultimate Promises in Unity", link: "/docs/upromises/" },
                ],
            },
            {
                text: "fivefingergames",
                link: "https://fivefingergames.com",
            },
            {
                text: "UE4 Marketplace",
                link: "https://www.unrealengine.com/marketplace/profile/fivefingergames",
            },
            {
                text: "GitHub",
                link: "https://github.com/fivefingergames",
            },
        ],
        sidebar: {
            "/docs/twitch-auth/": [
                {
                    title: "Twitch Auth",
                    collapsable: false,
                    children: ["", "installation", "usage"],
                },
            ],
            "/docs/upromises/": [
                {
                    title: "UPromises",
                    collapsable: false,
                    children: ["", "usage"],
                },
            ],
            "/docs/action-queue/": [
                {
                    title: "Action Queue",
                    collapsable: false,
                    children: ["", "action-queue-component", "action", "sub-action", "example"],
                },
            ],
            "/docs/coma/": [
                {
                    title: "Coma - Component Manager",
                    collapsable: false,
                    children: [""],
                },
                {
                    title: "Blueprint Usage",
                    collapsable: false,
                    children: [
                        "blueprints/managed-components",
                        "blueprints/defining-dependencies",
                        "blueprints/using-managed-components",
                        "blueprints/removing-managed-components",
                    ],
                },
                {
                    title: "Blueprint Example",
                    collapsable: false,
                    children: [
                        "example/blueprints/introduction",
                        "example/blueprints/board-component",
                        "example/blueprints/board-generator-component",
                        "example/blueprints/pawn-spawner-component",
                        "example/blueprints/wiring-dependencies",
                        "example/blueprints/its-alive",
                    ],
                },
                {
                    title: "C++ Usage",
                    collapsable: false,
                    children: ["cpp/creating-managed-components", "cpp/adding-dependencies", "cpp/opting-out-of-features", "cpp/using-managed-components"],
                },
                {
                    title: "C++ Example",
                    collapsable: false,
                    children: [
                        "example/cpp/introduction",
                        "example/cpp/board-component",
                        "example/cpp/board-generator-component",
                        "example/cpp/pawn-spawner-component",
                        "example/cpp/wiring-dependencies",
                        "example/cpp/its-alive",
                    ],
                },
            ],
        },
    },
    plugins: [
        [
            "vuepress-plugin-medium-zoom",
            {
                delay: 1000,
                options: {
                    margin: 24,
                    background: "#00131f28",
                    scrollOffset: 0,
                },
            },
        ],
    ],
};
