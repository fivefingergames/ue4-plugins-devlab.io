export default ({ router }) => {
    router.addRoutes([
        { path: "/twitch-auth/", redirect: "/docs/twitch-auth/" },
        { path: "/coma/", redirect: "/docs/coma/" },
        { path: "/upromises/", redirect: "/docs/upromises/" },
    ]);
};
